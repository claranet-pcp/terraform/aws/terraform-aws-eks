terraform {
  required_version = "~> 1.9.4"

  required_providers {
    aws = "~> 5.62.0"
  }
}
